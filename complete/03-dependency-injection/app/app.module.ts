import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { ContainerComponent } from './container/container.component';
import { CalculatorService } from './calculator.service';
import { SavedCalculationsService } from './saved-calculations.service';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    ContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [CalculatorService, SavedCalculationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
